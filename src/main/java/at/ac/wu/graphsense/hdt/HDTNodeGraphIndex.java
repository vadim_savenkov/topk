package at.ac.wu.graphsense.hdt;

import at.ac.wu.graphsense.rdf.RDFGraphIndex;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdtjena.HDTGraph;

/**
 * Created by vadim on 09.06.17.
 */
public class HDTNodeGraphIndex extends RDFGraphIndex {

    public HDTNodeGraphIndex(String dataset, boolean indexed) {
        super(new HDTGraph((new HDTIntGraphIndex(dataset,indexed)).getHDT(), true));
    }

    public HDTNodeGraphIndex(HDT hdt ){
        super(new HDTGraph(hdt, true));
    }

}
