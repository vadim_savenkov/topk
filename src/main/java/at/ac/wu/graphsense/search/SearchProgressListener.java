package at.ac.wu.graphsense.search;

import at.ac.wu.graphsense.Edge;

import java.io.IOException;
import java.util.List;

/**
 * Created by Vadim on 07.12.2016.
 */
public interface SearchProgressListener<V,E> {
    boolean onAdvance(int numVertices, int numEdges, int direction);
    boolean onPathFound(List<Edge<V,E>> path);
}
