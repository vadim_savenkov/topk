package at.ac.wu.graphsense.search;

import at.ac.wu.graphsense.*;
import at.ac.wu.graphsense.search.patheval.CumulativeRank;
import at.ac.wu.graphsense.search.patheval.PathArbiter;

import java.util.*;

/**
 * Created by vadim on 12.10.16.
 */
public class BidirectionalTopK<V,E> implements TopKSearchAlgorithm<V,E> {

    protected Set<V> startNodes, targetNodes;
    protected int k;
    protected PathArbiter<V,E> arbiter;
    protected GraphIndex<V,E> gix;

    final static int FORWARD = 0;
    final static int BACKWARD = 1;

    List<Frontier> ff;

    Collection<List<Edge<V,E>>> solutions;

    List<SearchProgressListener<V,E>> listeners = new LinkedList<>();

    public BidirectionalTopK( GraphIndex<V,E> graphIndex ){
        this.gix = graphIndex;
    }

    public void init(GraphIndex<V,E> gi){
        gix = gi;

        ff = new LinkedList<>();
        ff.add(null);
        ff.add(null);
        solutions = new LinkedList<>();
    }

    @Override
    public void setPathArbiter(PathArbiter<V,E> arbiter)
    {
        if(arbiter == null && gix == null){
            return;
        }
        this.arbiter = arbiter == null ? gix.createAllPassArbiter() : arbiter;
    }

    public void addProgressListener( SearchProgressListener<V,E> listener ){
        listeners.add(listener);
    }


    public Collection<List<Edge<V,E>>> run(V start, V end, int k){
        return run(Collections.singleton(start), Collections.singleton(end), k);
    }

    //@Loggable(prepend=true)
    public Collection<List<Edge<V,E>>> run( Collection<V> start, Collection<V> end, int k ) {

        init(gix);

        Map<Integer,Set<List<Edge<V,E>>>> solutionsByLength = new HashMap<>();
        startNodes = new HashSet<>(start);
        targetNodes = new HashSet<>(end);
        this.k = k;
        if( arbiter == null ){
            arbiter = gix.createAllPassArbiter();
        }
        arbiter.init(gix,startNodes,targetNodes,true);

        ff.set(FORWARD, new Frontier(false));
        for( V n : startNodes ){
            ff.get(FORWARD).add(makeTerminalEdge(n));
        }
        ff.set(BACKWARD, new Frontier(true));
        for ( V n: targetNodes ){
            ff.get(BACKWARD).add(makeTerminalEdge(n));
        }

        int direction=BACKWARD;
        int pathsFound = 0;

        while (!(ff.get(FORWARD).isEmpty() || ff.get(BACKWARD).isEmpty())) {

            direction = 1 - direction;

            ff.set(direction, ff.get(direction).advance());

            Frontier f = ff.get(direction);

            //Frontier size
            int numEdges = 0;
            for(Collection pathset : f.aix.values()) {
                numEdges += pathset.size();
            }

            for( SearchProgressListener l : listeners ){
                if(!l.onAdvance(f.vertices().size(), numEdges, direction)){
                    pathsFound = k;
                    break;
                }
            }

            //concatenate matching paths
            for( V v : ff.get(FORWARD).vertices() ){

                if( ff.get(FORWARD).hasPaths(v) && ff.get(BACKWARD).hasPaths(v) ) {

                    for( TraversalEdge<V,E> left : ff.get(FORWARD).paths(v) ) {

                        for ( TraversalEdge<V,E> right : ff.get(BACKWARD).paths(v) ){

                            double rank = this.arbiter.composeRanks(left, left.rank, right, right.rank);

                            if( !CumulativeRank.isPruneRank(rank) ){
                                LinkedList<Edge<V,E>> path = new LinkedList<>();

                                left.traceLeft(path);
                                right.traceRight(path);

                                //avoid repeated edges
                                Iterator<Edge<V,E>> it = path.iterator();
                                Set<String> edges = new HashSet<>();
                                Edge<V,E> prev = it.next();
                                while( it.hasNext() ){
                                    Edge<V,E> cur = it.next();
                                    String edge = String.format("%s:-:%s:-:%s"
                                            ,prev.vertex(),cur.label(),cur.vertex());

                                    if( edges.contains(edge) ){
                                        rank = CumulativeRank.PRUNE_PATH;
                                        break;
                                    }
                                    edges.add(edge);
                                    prev = cur;
                                }

                                if( !CumulativeRank.isPruneRank(rank) ) {

                                    if( !solutionsByLength.containsKey(path.size()) ){
                                        solutionsByLength.put(path.size(),new HashSet<List<Edge<V, E>>>());
                                    }
                                    int oldSize = solutionsByLength.get(path.size()).size();
                                    solutionsByLength.get(path.size()).add(path);
                                    // it may happen that duplicate paths are found, do not count those
                                    int delta = solutionsByLength.get(path.size()).size() - oldSize;

                                    if( delta > 0 ) {
                                        pathsFound += delta;

                                        for (SearchProgressListener l : listeners) {
                                            if (!l.onPathFound(path)) {
                                                return flattenResults(solutionsByLength);
                                            }
                                        }
                                    }
                                    if (pathsFound >= k) {
                                        return flattenResults(solutionsByLength);
                                    }

                                }

                            }

                        }
                    }
                }
            }
        }
        return flattenResults(solutionsByLength);
    }

    Collection<List<Edge<V,E>>> flattenResults( Map<Integer,Set<List<Edge<V,E>>>> solutionsByLength ){
        List<Integer> lengths = new LinkedList<>(solutionsByLength.keySet());
        Collections.sort(lengths);

        for( int l : lengths ){
            for( List<Edge<V,E>> path : solutionsByLength.get(l) ){
                solutions.add(path);
            }
        }
        return solutions;
    }

    TraversalEdge<V,E> makeTerminalEdge( V vertex ){
        boolean backward = this.targetNodes.contains(vertex);
        TraversalEdge<V,E> te = new TraversalEdge<>();
        Edge<V,E> e = new Edge<>(vertex,null);
        te.init( e, null, new CumulativeRank(0, arbiter.getInitialState(backward)), backward );
        return te;
    }



    //
    class Frontier {
        // Affix (i.e., prefix or suffix) index -- by the end vertex
        Map<V, List<TraversalEdge<V,E>>> aix = new HashMap<>();
        boolean backward;

        Frontier(boolean backward){
            this.backward = backward;
        }

        void add(TraversalEdge<V,E> te){
            V v = te.vertex();
            if ( !aix.containsKey(v)){
                aix.put(v, new LinkedList<TraversalEdge<V,E>>());
            }
            aix.get(v).add(te);
        }

        Set<V> vertices(){ return aix.keySet(); }

        boolean hasPaths(V vertex) {
            return aix.containsKey(vertex);
        }

        List<TraversalEdge<V,E>> paths( V vertex ){
            return aix.get(vertex);
        }

        Frontier advance(){
            Frontier f = new Frontier(this.backward);
            for( V v : aix.keySet() ){
                List<TraversalEdge<V,E>> tes = aix.get(v);

                for(TraversalEdge<V,E> te : tes ) {

                    List<TraversalEdge<V, E>> adj = te.adjacentEdges(gix, arbiter, this.backward);

                    for (TraversalEdge<V, E> sprout : adj) {
                        f.add(sprout);
                    }

                }
            }
            return f;
        }

        boolean isEmpty(){ return aix.isEmpty(); }
    }


    static class TraversalEdge<V,E> implements Iterable<Edge<V,E>> {
        CumulativeRank rank;
        Edge<V,E> e;
        TraversalEdge<V,E> prev = null;
        boolean backward = false; //for debugging (swapping end<->start in toString()) only

        //        @Loggable(skipArgs = true)
        public void init( Edge<V,E> edge, TraversalEdge prev, CumulativeRank rank, boolean backward ){
            this.e = edge;
            this.prev = prev;
            this.rank = rank;
            this.backward = backward;
        }

        public V vertex(){
            return e.vertex();
        }

        // @Loggable(skipArgs = true)
        public List<Edge<V,E>> traceLeft(LinkedList<Edge<V,E>> trace){
            trace.addFirst(e);
            return prev==null? trace : prev.traceLeft(trace);
        }

        // @Loggable(skipArgs = true)
        public List<Edge<V,E>> traceRight(List<Edge<V,E>> trace){
            if( prev==null ){
                return trace;
            }
            trace.add(new Edge<V,E>(prev.vertex(),e.label()));
            return prev.traceRight(trace);
        }

        public boolean visitedEdge(V source, Edge<V,E> edge) {
            if (prev != null) {
                if( Objects.equals(e, edge) && Objects.equals(prev.vertex(), source) ){
                    return true;
                }
                return prev.visitedEdge(source,edge);
            }
            return false;
        }

        public Iterator<Edge<V,E>> iterator(){
            return new Iter<V,E>(this);
        }


        public List<TraversalEdge<V,E>> adjacentEdges(GraphIndex gix, PathArbiter arb, boolean backward){
            List<TraversalEdge<V,E>> es = new LinkedList<>();

            Iterator<Edge<V,E>> edges = backward? gix.lookupEdges(null, e.vertex())
                    : gix.lookupEdges(e.vertex(), null);
            boolean fork = false;
            while (edges.hasNext()) {
                Edge<V,E> sprout = edges.next();

                fork = fork || edges.hasNext();

                CumulativeRank rank = arb.rankEdge(sprout, this, this.rank, fork, backward );

                if( !rank.isPruneRank() && !visitedEdge(e.vertex(),sprout) ) {
                    TraversalEdge<V,E> te = new TraversalEdge<>();
                    te.init(sprout, this, rank, backward);
                    es.add(te);
                }
            }
            return es;
        }

        @Override
        public String toString(){
            String sv = "("+e.vertex()+")";
            if( prev==null ){ return "["+sv+"]"; }
            String start = "("+prev.vertex()+")", end = sv;

            if(this.backward){ //swap direction (why we need the backward switch in the class at all)
                sv = start; start = end; end = sv;
            }

            return start + "-"+ e.label() + "-" + end;
        }

        static class Iter<V,E> implements Iterator<Edge<V,E>>{
            TraversalEdge<V,E> te;

            public Iter( TraversalEdge<V,E> te ){this.te = te;}

            public boolean hasNext(){ return te!=null && te.prev!=null; }

            public Edge<V,E> next(){
                Edge<V,E> ret = te.e;
                te = te.prev;
                return ret;
            }

            public void remove(){
                throw new UnsupportedOperationException();
            }
        }

    }

}
